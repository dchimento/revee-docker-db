#!/bin/bash
mysqldump --lock-tables=false --insert-ignore   -h ${1:?}  -P 33062 -u $2 -p$3 --skip-triggers  --databases revee  --ignore-table=revee.activity_log --ignore-table=revee.amplify_campaign_configuration --ignore-table=revee.amplify_promoted_link > revee.sql

mysqldump --lock-tables=false --no-data --insert-ignore   -h ${1:?}  -P 33062 -u $2 -p$3 --skip-triggers  --databases analytics  --ignore-table=revee.activity_log --ignore-table=revee.amplify_campaign_configuration --ignore-table=revee.amplify_promoted_link --ignore-table=revee.third_party_rpm > analytics.sql

mysqldump --lock-tables=false --no-data --insert-ignore   -h ${1:?}  -P 33062 -u $2 -p$3 --skip-triggers  --databases reporting > reporting.sql

mysqldump --lock-tables=false --no-data --insert-ignore   -h ${1:?}  -P 33062 -u $2 -p$3 --skip-triggers  --databases tvr  > tvr.sql
