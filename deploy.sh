#!/bin/bash
source config.sh
./build.sh "$@"

docker login -u ${artifactoryUser} -p ${artifactoryPassword}  accessmc-docker-local.jfrog.io
docker tag $FORCE revee_db accessmc-docker-local.jfrog.io/revee_db
docker push accessmc-docker-local.jfrog.io/revee_db
